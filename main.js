"use strict";
var menu = document.getElementById('menu');
var nav = document.getElementById('navShow');
var header = document.getElementById('header');
var container = document.querySelector(".homecontainer");

menu.addEventListener('click',mobileMenu);

function mobileMenu() {
  nav.classList.toggle("list-show");
  menu.classList.toggle("open");
};

window.addEventListener('scroll', changeOpacity);

function changeOpacity() {
  if(document.body.scrollTop > header.offsetHeight){
    header.classList.add('opacity');
  } else {
    header.classList.remove('opacity');
  }
};


var currentSlideClass = "slide-one";
var currentButtonId = "one";

var image1 = new Image();
image1.src = "images/Pueblo.jpg";
var image2 = new Image();
image2.src = "images/Manos.jpg";
var image3 = new Image();
image3.src = "images/Montanas.jpg";


function changeOne(event) {
  var currentButton = event.currentTarget;
  var id = currentButton.getAttribute('id');

  if (id !== currentButtonId) {
    var previousButton = document.getElementById(currentButtonId);
    var nextSlideClass = "slide-" + id;

    previousButton.classList.remove("btn-active");
    currentButton.classList.add("btn-active");
    container.classList.remove(currentSlideClass);
    container.classList.add(nextSlideClass);

    currentButtonId = id;
    currentSlideClass = nextSlideClass;
  }

};

function footer(){
  alert('hola');
  $(".footer").append("<div class="col-sm-4">.col-sm-4</div>");
  $(".footer").append("<div class="col-sm-4">.col-sm-4</div>");
  $(".footer").append("<div class="col-sm-4">.col-sm-4</div>");
};
